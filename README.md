# Los Santos Police Database

Project of Police Database created for SA:MP server to LSPD fraction.
Written in language PHP using framework Symfony 4.3.

Based on MIT License.

Site contains:
- User accounts with roles (user, mod, admin) and special permissions,
- Personal files with entries (criminal and traffic offenses),
- Simple Admin Control Panel with accounts management and user logs.